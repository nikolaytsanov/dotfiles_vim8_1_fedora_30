CWD=`pwd`
cd ~/python/bookware-pos
rm -rf ~/python/uml/*
epydoc -v -o ~/python/uml --name bookware-pos \
	 --inheritance listed --graph all --parse-only \
	./*.py \
	./bank/*.py \
	./bank/moneris_emv/*.py \
	./bank/moneris_emv/libs/*.py \
	./bank/paymentech_emv/*.py \
	./bank/paymentech_emv/libs/*.py \
	./bank/td_emv/*.py \
	./bank/td_emv/libs*.py \
	./bank/td_noire510/*.py \
	./bank/tender_retail/*.py \
	./bank/tender_retail_paymentech/*.py \
	./bank/tender_retail_paymentech/libs/*.py \
	./bank/tender_retail_td/*.py \
	./customers/*.py \
	./customers/tanda/*.py \
	./customers/tanda/config/*.py \
	./customers/tanda/config/slips/*.py \
	./drivers/*.py \
	./drivers/2.6.27-11-generic/*.py \
	./drivers/2.6.32-21-generic/*.py \
	./graphics/*.py \
	./images/*.py \
	./loadprogs/*.py \
	./logs/*.py \
	./paymentinterface/*.py \
	./ped/*.py \
	./ped/td_noire510/*.py \
	./printer/*.py \
	./schema/*.py \
	./scripts/*.py \
	./sound/*.py \
	./tender/*.py \
	./tests/*.py \
	./third_party/bw6_client/*.py \
	./third_party/bw6_client/config/*.py \
	./third_party/bw6_client/lib/*.py \
	./third_party/bw6_client/messages/*.py \
	./ui/*.py \
	./virtualaction/*.py \
	./config/*.py 
cd $CWD
