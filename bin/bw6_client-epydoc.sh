CWD=`pwd`
cd /home/SENECA/SCRIPTS/bw6_client
rm -rf ~/python/uml/*
epydoc -v -o ~/python/uml --name bw6_client \
	 --inheritance listed --graph all --parse-only \
	./*.py \
	./config/*.py \
	./lib/*.py \
	./messages/*.py
cd $CWD
