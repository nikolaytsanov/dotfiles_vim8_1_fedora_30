### Plugins for Python development under Fedora Core 30
* vim-projectionist
* tagbar
* ctrl-p
* airline
* buffer gator
* tagbar-phpctags
* phpcomplete
* YouCompleteMe
* python-mode - <C-C>g - to go to definition of the identifier under the cursor, <C-S> autocomplete an idenfier you started, :PymodeLint, then /error in the pylint output buffer
* vim-dispatch - :Dispatch grep -ir posmain * would give me a buffer with all files which contain the string 'posmain'. Fills the lack of search in all files (Ctrl+Shift+F in eric)

python-mode uses python rope which creates a .ropeproject directory with settings and cache. When features like code navigation stop working, we can edit these settings or simply delete this directory and reinitialize rope (it'll happen automatically).
